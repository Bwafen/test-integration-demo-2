package fr.baobab.testIntegrationDemo2.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.baobab.testIntegrationDemo2.domain.UserRessource;
import fr.baobab.testIntegrationDemo2.domain.model.User;
import fr.baobab.testIntegrationDemo2.service.RegisterUseCase;
@WebMvcTest
class RegisterRestControllerTest {
	@Autowired
	MockMvc mockMvc;
	@Autowired 
	ObjectMapper objectMapper;
	@MockBean
	RegisterUseCase registerUseCase;
	@Test
	void whenInputValid_thenReturn200() throws Exception {
		UserRessource user = new UserRessource(null, "Soupra", "soupra@test.fr");
		mockMvc.perform(post("/forum/1/register?sendResponseMail=1")
							.contentType("application/json").content(objectMapper.writeValueAsString(user)))
								.andExpect(status().isOk());
	}
	
	@Test
	void whenInputValid_thenCallBusinessLogic() throws Exception {
		UserRessource user = new UserRessource(null, "Soupra", "soupra@test.fr");
		mockMvc.perform(post("/forum/1/register?sendResponseMail=1")
							.contentType("application/json").content(objectMapper.writeValueAsString(user)));
		
		ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
		verify(registerUseCase, times(1)).registerUser(captor.capture(), eq(true));
		assertThat(captor.getValue().getName()).isEqualTo("Soupra");
		assertThat(captor.getValue().getEmail()).isEqualTo("soupra@test.fr");
	}
	
	@Test
	void whenInputValid_thenReturnUserRessource() throws Exception {
		UserRessource user = new UserRessource(0L, "Soupra", "soupra@test.fr");
		MvcResult resultat = mockMvc.perform(post("/forum/1/register?sendResponseMail=1")
							.contentType("application/json").content(objectMapper.writeValueAsString(user)))
								.andReturn();
		String resultatServeur = resultat.getResponse().getContentAsString();
		UserRessource resultatAttendu = new UserRessource(0L, "Soupra", "soupra@test.fr");
		String resultatAttenduString = objectMapper.writeValueAsString(resultatAttendu);
		assertThat(resultatServeur).isEqualTo(resultatAttenduString);
		
	}
	
}
