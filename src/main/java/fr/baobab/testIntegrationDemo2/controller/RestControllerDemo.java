package fr.baobab.testIntegrationDemo2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestControllerDemo {
	@GetMapping("/rest/demo")
	public List<String> listString(){
		List<String> li = new ArrayList<String>();
		li.add("Luisa");
		li.add("Franck");
		li.add("Tatjana");
		return li;
	}
}
