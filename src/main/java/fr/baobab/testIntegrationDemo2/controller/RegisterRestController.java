package fr.baobab.testIntegrationDemo2.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.baobab.testIntegrationDemo2.domain.UserRessource;
import fr.baobab.testIntegrationDemo2.domain.model.User;
import fr.baobab.testIntegrationDemo2.service.RegisterUseCase;

@RestController
public class RegisterRestController {
	@Autowired
	RegisterUseCase registerUseCase;
	@PostMapping("/forum/{forumId}/register") // ex: /forum/2/register?sendResponseMail=1 
	public UserRessource register(@PathVariable Integer forumId, 
									@Valid @RequestBody UserRessource userRessource,
									@RequestParam boolean sendResponseMail) {
		User user = new User(null, userRessource.getUserName(), userRessource.getUserEmail());
		Long userId = registerUseCase.registerUser(user , sendResponseMail);
		return new UserRessource (userId , user.getName (), user.getEmail());
	}
}
