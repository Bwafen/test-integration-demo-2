package fr.baobab.testIntegrationDemo2.service;

import fr.baobab.testIntegrationDemo2.domain.model.User;

public interface RegisterUseCase {
	Long registerUser(User user, boolean sendWelcomeMail);
}
