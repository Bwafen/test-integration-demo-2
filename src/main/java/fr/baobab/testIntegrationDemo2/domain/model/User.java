package fr.baobab.testIntegrationDemo2.domain.model;

import lombok.Data;
import lombok.Value;

@Data
public class User {
	final private Long id;
	final private String name;
	final private String email;
	
}
